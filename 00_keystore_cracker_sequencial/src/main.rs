use openssl::pkcs12::*;
use std::{str};
mod base_counter;
use crate::base_counter::base_counter::BaseCounter;
use std::fs::File;
use std::io::Read;
use std::process;

fn main() {
    const ALFABETH: [&str; 10] = [
        "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
    ];

    let alfabet = to_string_vector(Box::from(ALFABETH));
    let counter = BaseCounter::new(6, alfabet.len());
    let pkcs12 = open_file("keystore.p12");

    let mut counting = 0;

    // Functional style
    counter
        .into_iter()
        .for_each(|state| {
            let password = combination(&alfabet, &state).concat();
            check_password(&state, &password, &pkcs12);
            feedback(&mut counting, &state, &password);
    });


    // Imperative style
    // for state in counter {
    //     let password = combination(&alfabet, &state).concat();
    //     check_password(&state, &password, &pkcs12);
    //     feedback(&mut counting, &state, &password);
    // }


}

fn check_password(state: &Vec<usize>, password: &str, pkcs12: &Pkcs12) {
    if pkcs12.parse(&password).is_ok() {
        println!("Found password: {:?} for inner-state {:?}", password, state);
        println!("Certificate: {:?}", pkcs12.parse(&password).unwrap().cert);
        process::exit(1);
    }
}

fn feedback(counting: &mut usize, state: &Vec<usize>, password: &str) {
    if *counting > 0 {
        *counting = 0;
        println!(
            "Trying password: {:?} for inner-state {:?}",
            password, state
        );
    }
    *counting += 1;
}

fn to_string_vector(items: Box<[&str]>) -> Vec<String> {
    let alfabet = items.iter().map(|x| x.to_string()).collect::<Vec<String>>();
    alfabet
}

fn combination(alfabet: &Vec<String>, combination: &Vec<usize>) -> Vec<String> {
    combination
        .iter()
        .map(|&number| alfabet.get(number).unwrap().clone())
        .collect()
}

fn open_file(filename: &str) -> Pkcs12 {
    // https://docs.rs/openssl/0.9.1/openssl/ssl/index.html
    let mut file = File::open(filename).unwrap();
    let mut pkcs12 = vec![];
    file.read_to_end(&mut pkcs12).unwrap();
    Pkcs12::from_der(&pkcs12).unwrap()
}
