pub struct BaseCounter {
    base: usize,
    length: usize,
    state: Vec<usize>,
    first_time: bool,
}

#[allow(dead_code)]
impl BaseCounter {
    pub fn new(length: usize, base: usize) -> Self {
        BaseCounter {
            length,
            base,
            state: vec![0; length],
            first_time: true,
        }
    }

    pub fn length(&self) -> usize {
        self.length
    }

    pub fn current_base(&self) -> usize {
        self.base
    }

    pub fn current_state(&self) -> &Vec<usize> {
        self.state.as_ref()
    }

    pub fn set_state(&mut self, state: Vec<usize>) -> Result<bool, &'static str> {
        match state.len() == self.length {
            true => {
                self.state = state;
                Ok(true)
            }
            false => {
                // TODO: add the current length.
                Err("Wrong size for new state")
            }
        }
    }

    fn add_one(&mut self) {
        match self.first_time {
            true => self.first_time = false,
            false => self.state[0] += 1,
        }
    }

    fn propagate(&mut self) -> Result<(), &'static str> {
        for index in 0..self.length - 1 {
            if !Self::is_bigger_than_base(self, self.state[index]) {
                continue;
            }

            self.state[index] -= self.base;

            if index + 1 >= self.length {
                continue;
            }

            let result = self.state[index + 1] + 1;

            match index + 1 == self.length - 1 {
                true => match Self::is_bigger_than_base(self, result) {
                    true => return Err("overflow"),
                    false => self.state[index + 1] = result,
                },
                false => self.state[index + 1] = result,
            }
        }
        Ok(())
    }

    fn is_bigger_than_base(&self, number: usize) -> bool {
        number > self.base - 1
    }
}

impl Iterator for BaseCounter {
    type Item = Vec<usize>;

    fn next(&mut self) -> Option<Self::Item> {
        self.add_one();

        let result = self.propagate();
        if result.is_ok() {
            Option::from(self.current_state().to_vec())
        } else {
            None
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_instantiation() {
        let counter = BaseCounter::new(5, 10);

        assert_eq!(counter.current_state().len(), 5);
        assert_eq!(counter.current_state().to_owned(), vec![0, 0, 0, 0, 0]);
        assert_eq!(counter.current_base(), 10);
    }

    #[test]
    fn test_setting_and_getting_state() {
        let mut counter = BaseCounter::new(2, 10);

        let result = counter.set_state(vec![2, 3]);
        assert_eq!(result, Ok(true));
        let current_state = counter.current_state();
        assert_eq!(vec![2, 3], current_state.to_owned());
        assert_eq!(2, counter.length());
    }

    #[test]
    fn test_setting_state_failure() {
        let mut counter = BaseCounter::new(3, 10);

        let result = counter.set_state(vec![2, 3, 3, 4]);
        assert_eq!(result, Err("Wrong size for new state"));
        let current_state = counter.current_state();
        assert_eq!(3, current_state.to_owned().len());
        assert_eq!(3, counter.length());
    }

    #[test]
    fn test_setting_state() {
        let mut counter = BaseCounter::new(3, 10);

        let result = counter.set_state(vec![8, 9, 9]);
        assert_eq!(result, Ok(true));

        let current_state = counter.current_state().to_owned();
        assert_eq!(current_state, vec![8, 9, 9]);

        assert_eq!(counter.next(), Some(vec![8, 9, 9]));
        assert_eq!(counter.next(), Some(vec![9, 9, 9]));
        assert_eq!(counter.next(), None);
    }
}
