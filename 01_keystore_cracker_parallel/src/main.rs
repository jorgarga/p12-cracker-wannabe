use openssl::pkcs12::*;
use std::sync::MutexGuard;
use std::sync::{Arc, Mutex};
use std::{str, thread};
mod base_counter;
use crate::base_counter::base_counter::BaseCounter;
use std::fs::File;
use std::io::Read;
use std::process;
use std::ops::Deref;

fn main() {
    const ALFABETH: [&str; 10] = [
        /* "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", */
        "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
    ];

    let alfabet = to_string_vector(Box::from(ALFABETH));
    let counter = Arc::new(Mutex::new(BaseCounter::new(6, alfabet.len())));
    let counting = Arc::new(Mutex::new(0));
    let pkcs12 = Arc::new(open_file("keystore.p12"));

    loop {
        let mut thread_handles = vec![];
        for _ in 0..100 {
            let counter_intern = Arc::clone(&counter);
            let counting_intern = Arc::clone(&counting);
            let pkcs12_intern = Arc::clone(&pkcs12);

            let handle = thread::spawn(move || {
                let mut copy = counter_intern.lock().unwrap();
                copy.next();
                let state = copy.current_state();
                let alfabet = to_string_vector(Box::from(ALFABETH));
                let password = combination(&alfabet, &state).concat();

                check_password(state, &password, pkcs12_intern.deref());
                feedback(counting_intern.lock().unwrap(), &state, &password);
            });
            thread_handles.push(handle);
        }

        for handle in thread_handles {
            handle.join().unwrap();
        }
    }

}

fn check_password(state: &Vec<usize>, password: &str, pkcs12: &Pkcs12) {
    if pkcs12.parse(&password).is_ok() {
        println!("Found password: {:?} for inner-state {:?}", password, state);
        println!("Certificate: {:?}", pkcs12.parse(&password).unwrap().cert);
        process::exit(1);
    }
}

fn feedback(mut counting: MutexGuard<i32>, state: &Vec<usize>, password: &str) {
    if *counting > 0 {
        *counting = 0;
        println!(
            "Trying password: {:?} for inner-state {:?}",
            password, state
        );
    }
    *counting += 1;
}

fn to_string_vector(items: Box<[&str]>) -> Vec<String> {
    let alfabet = items.iter().map(|x| x.to_string()).collect::<Vec<String>>();
    alfabet
}

fn combination(alfabet: &Vec<String>, combination: &Vec<usize>) -> Vec<String> {
    combination
        .iter()
        .map(|&number| alfabet.get(number).unwrap().clone())
        .collect()
}

fn open_file(filename: &str) -> Pkcs12 {
    // https://docs.rs/openssl/0.9.1/openssl/ssl/index.html
    let mut file = File::open(filename).unwrap();
    let mut pkcs12 = vec![];
    file.read_to_end(&mut pkcs12).unwrap();
    Pkcs12::from_der(&pkcs12).unwrap()
}
