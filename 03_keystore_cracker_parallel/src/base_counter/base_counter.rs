#[derive(Debug, PartialEq)]
pub struct BaseCounter {
    base: usize,
    length: usize,
    state: Vec<usize>,
}

impl BaseCounter {
    pub fn new(base: usize, length: usize, state: Vec<usize>) -> Self {
        BaseCounter {
            base,
            length,
            state,
        }
    }

    pub fn current_base(&self) -> usize {
        self.base
    }

    pub fn current_state(&self) -> &Vec<usize> {
        self.state.as_ref()
    }

    fn add_one(&self) -> Result<Vec<usize>, &'static str> {
        let mut new_state = self.state.clone();
        new_state[0] += 1;

        for index in 0..self.length - 1 {
            if new_state[index] < self.base {
                continue;
            }

            new_state[index] -= self.base;

            if index + 1 >= self.length {
                continue;
            }

            let result = new_state[index + 1] + 1;

            match index + 1 == self.length - 1 {
                true => match result > self.base - 1 {
                    true => return Err("overflow"),
                    false => new_state[index + 1] = result,
                },
                false => new_state[index + 1] = result,
            }
        }
        Ok(new_state)
    }
}

impl Iterator for BaseCounter {
    type Item = BaseCounter;

    fn next(&mut self) -> Option<Self::Item> {
        let new_state = self.add_one();

        if let Ok(state) = new_state {
            Option::from(BaseCounter {
                base: self.base,
                length: self.length,
                state: state,
            })
        } else {
            None
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_instantiation() {
        let counter = BaseCounter::new(10, 5, vec![0, 0, 0, 0, 0]);

        assert_eq!(counter.current_state().len(), 5);
        assert_eq!(counter.current_state().to_owned(), vec![0, 0, 0, 0, 0]);
        assert_eq!(counter.current_base(), 10);
    }

    #[test]
    fn test_setting_and_getting_state() {
        let counter = BaseCounter::new(10, 2, vec![0, 0, 0, 1, 0]);

        let current_state = counter.current_state();
        assert_eq!(vec![0, 0, 0, 1, 0], current_state.to_owned());
    }

    #[test]
    fn test_always_get_same_next_state() {
        let mut counter = BaseCounter::new(10, 3, vec![0, 0, 1]);

        assert_eq!(counter.next().unwrap().state, vec![1, 0, 1]);
        assert_ne!(counter.next().unwrap().state, vec![2, 0, 1]);
        assert_eq!(counter.next().unwrap().state, vec![1, 0, 1]);
    }

    #[test]
    fn test_10_comes_after_9_step() {
        let mut counter = BaseCounter::new(10, 3, vec![9, 0, 1]);
        let counter2 = counter.next().unwrap();
        assert_eq!(counter2.state, vec![0, 1, 1]);
    }

    #[test]
    fn test_overflow_step() {
        let mut counter = BaseCounter::new(10, 3, vec![9, 9, 9]);
        let counter2 = counter.next();
        assert_eq!(counter2, None);
    }

    #[test]
    fn test_state_loop() {
        let mut counter = BaseCounter::new(10, 3, vec![0, 0, 1]);

        for _ in 0..200 {
            let counter2 = counter.next().unwrap();
            counter = counter2;
        }

        assert_eq!(counter.state, vec![0, 0, 3]);
    }
}
