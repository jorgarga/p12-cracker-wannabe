// const LATIN: [&str; 52] = [
//     "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S",
//     "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l",
//     "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
// ];
//
// const NUMBERS: [&str; 10] = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
//
// const EXTENDED_ASCII: [&str; 10] = ["Ñ", "Ç", "Æ", "Ø", "Å", "ñ", "ç", "æ", "ø", "å"];
//
// const SPECIAL: [&str; 32] = [
//     " ", "\"", "'", "!", "·", "$", "%", "&", "/", "(", ")", "=", "|", "@", "#", "~", "¬", "{", "[",
//     "]", "}", "\\", "+", "-", ".", ",", "¨", ";", ":", "_", "*", "^",
// ];
//
use openssl::pkcs12::*;
use std::sync::{Arc, RwLock};
use std::{str, thread};
mod base_counter;
use crate::base_counter::base_counter::BaseCounter;
use std::fs::File;
use std::io::Read;
use std::ops::Deref;
use std::process;

fn main() {
    const ALFABETH: [&str; 10] = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];

    let alfabet = Arc::new(to_string_vector(Box::from(ALFABETH)));
    let mut counter = BaseCounter::new(alfabet.len(), 6, vec![0; 6]);
    let pkcs12 = Arc::new(open_file("keystore.p12"));

    loop {
        let mut handles = vec![];

        for _ in 0..100 {
            let alfabet_intern = Arc::clone(&alfabet);
            let pkcs12_intern = Arc::clone(&pkcs12);

            let mut counter2 = counter.next().unwrap();
            counter = counter2.next().unwrap();

            let handle = thread::spawn(move || {
                let password = combination(&alfabet_intern, &counter2.current_state()).concat();
                check_password(counter2.current_state(), &password, pkcs12_intern.deref());
                feedback(&counter2.current_state(), &password);
            });

            handles.push(handle);
        }

        for handle in handles {
            handle.join().unwrap();
        }
    }

    // Previous experiment:
    // -------------------------------------------------------------------------------------
    //     let alfabet = Arc::new(to_string_vector(Box::from(ALFABETH)));
    //     let counter = Arc::new(RwLock::new(BaseCounter::new(6, alfabet.len())));
    //     let pkcs12 = Arc::new(open_file("keystore.p12"));
    //
    //     loop {
    //         let mut thread_producer_handles = vec![];
    //         let mut thread_reader_handles = vec![];
    //
    //         for _ in 0..15000 {
    //             let counter_producer = Arc::clone(&counter);
    //             let counter_reader = Arc::clone(&counter);
    //             let pkcs12_intern = Arc::clone(&pkcs12);
    //             let alfabet_intern = Arc::clone(&alfabet);
    //
    //             let producer_handle = thread::spawn(move || {
    //                 if let Ok(mut to_write) = counter_producer.write() {
    //                     to_write.next();
    //                 }
    //             });
    //             thread_producer_handles.push(producer_handle);
    //
    //             let reader_handle = thread::spawn(move || {
    //                 if let Ok(to_read) = counter_reader.read() {
    //                     let state = to_read.current_state();
    //
    //                     let password = combination(&alfabet_intern, &state).concat();
    //                     check_password(state, &password, pkcs12_intern.deref());
    //                     feedback(&state, &password);
    //                 }
    //             });
    //             thread_reader_handles.push(reader_handle);
    //         }
    //
    //         for handle in thread_producer_handles {
    //             handle.join().unwrap();
    //         }
    //
    //         for handle in thread_reader_handles {
    //             handle.join().unwrap();
    //         }
    //     }
}

fn check_password(state: &Vec<usize>, password: &str, pkcs12: &Pkcs12) {
    if pkcs12.parse(&password).is_ok() {
        println!("Found password: {:?} for inner-state {:?}", password, state);
        println!("Certificate: {:?}", pkcs12.parse(&password).unwrap().cert);
        process::exit(1);
    }
}

fn feedback(state: &Vec<usize>, password: &str) {
    println!(
        "Trying password: {:?} for inner-state {:?}",
        password, state
    );
}

fn to_string_vector(items: Box<[&str]>) -> Vec<String> {
    items.iter().map(|x| x.to_string()).collect::<Vec<String>>()
}

fn combination(alfabet: &Vec<String>, combination: &Vec<usize>) -> Vec<String> {
    combination
        .iter()
        .map(|&number| alfabet.get(number).unwrap().clone())
        .collect()
}

fn open_file(filename: &str) -> Pkcs12 {
    // https://docs.rs/openssl/0.9.1/openssl/ssl/index.html
    let mut file = File::open(filename).unwrap();
    let mut pkcs12 = vec![];
    file.read_to_end(&mut pkcs12).unwrap();
    Pkcs12::from_der(&pkcs12).unwrap()
}
