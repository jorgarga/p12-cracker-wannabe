use rayon::prelude::*;
use openssl::pkcs12::*;
use std::sync::Arc;
use std::{str};
mod base_counter;
use crate::base_counter::base_counter::BaseCounter;
use std::fs::File;
use std::io::Read;
use std::ops::Deref;
use std::process;

fn main() {

    const LATIN_BIG: &[&str] = &[
        "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S",
        "T", "U", "V", "W", "X", "Y", "Z"
    ];

    const EXTENDED_ASCII: &[&str] = &["Ñ", "Ç", "Æ", "Ø", "Å", "ñ", "ç", "æ", "ø", "å"];

    const SPECIAL: &[&str] = &[
        " ", "\"", "'", "!", "·", "$", "%", "&", "/", "(", ")", "=", "|", "@", "#", "~", "¬", "{", "[",
        "]", "}", "\\", "+", "-", ".", ",", "¨", ";", ":", "_", "*", "^",
    ];

    const CIRILYC: &[&str] = &[
        "а", "б", "в", "г", "д", "е", "ж", "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т",
        "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ь", "ю", "я",
    ];

    const NUMBERS: &[&str] = &["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];

    const LATIN_SMALL: &[&str] = &[
    "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l",
    "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];

    let alfabeth1 = slice_join(NUMBERS, LATIN_SMALL);
    let alfabeth2 = slice_join(CIRILYC, SPECIAL);
    let alfabeth3 = slice_join(LATIN_BIG, EXTENDED_ASCII);
    let alfabeth12 = slice_join(&alfabeth1, &alfabeth2);
    let alfabeth = slice_join(&alfabeth12, &alfabeth3);

    println!("ALFABETH: {:?}", alfabeth);

    let password_length = 6;
    let alfabet = to_string_vector(alfabeth);
    let pkcs12 = open_file("keystore.p12");

    let base = BaseCounter::new(alfabet.len(), password_length, vec![0; password_length]);
    base.into_iter()
        .par_bridge()
        .for_each(|base_counter|
            verify(&alfabet, &pkcs12, base_counter.current_state())
        );

}

fn verify(alfabet: &Vec<String>, pkcs12: &Pkcs12, current_state: &[usize]) {
    let password = combination(alfabet, current_state).concat();
    check_password(current_state, &password, pkcs12);
    feedback(current_state, &password);
}

fn check_password(state: &[usize], password: &str, pkcs12: &Pkcs12) {
    if pkcs12.parse(password).is_ok() {
        println!("Found password: {:?} for inner-state {:?}", password, state);
        println!("Certificate: {:?}", pkcs12.parse(password).unwrap().cert);
        process::exit(1);
    }
}

fn feedback(state: &[usize], password: &str) {
    println!(
        "Trying password: {:?} for inner-state {:?}",
        password, state
    );
}

fn to_string_vector(items: Vec<&str>) -> Vec<String> {
    items.par_iter().map(|x| x.to_string()).collect::<Vec<String>>()
}

fn combination(alfabet: &[String], combination: &[usize]) -> Vec<String> {
    combination
        .par_iter()
        .map(|&number| alfabet.get(number).unwrap().clone())
        .collect()
}

fn slice_join<T: Clone>(a: &[T], b: &[T]) -> Vec<T> {
    let mut v = a.to_vec();
    v.extend_from_slice(b);
    v
}

fn open_file(filename: &str) -> Pkcs12 {
    // https://docs.rs/openssl/0.9.1/openssl/ssl/index.html
    let mut file = File::open(filename).unwrap();
    let mut pkcs12 = vec![];
    file.read_to_end(&mut pkcs12).unwrap();
    Pkcs12::from_der(&pkcs12).unwrap()
}
