# p12 cracker

### Note
> This is the result of some days experimenting with Rust's **fearless concurrency**,
> 
> This code is shown for **learning** purposes.

The exercise consists on finding the lost password which successfully opens a [p12](https://en.wikipedia.org/wiki/PKCS_12) file.

The heart of the program is the _BaseCounter_ structure which is responsible for generating a number combination which later on will be translated into a password to be tried.
The state is kept in that structure so that the search can continue later if stopped.

There are 4 versions of the program:
1. Sequencial.
2. Parallel with Mutex
3. Parallel with RWLock
4. Parallel with Arc. BaseCounter is modified

All can be run with the command:
> time cargo run --release

## Sequencial
In this version the _BaseCounter_ structure uses an internal vector to hold state, to remember where we have been and where we are going to. It implements the Iterator Trait and just counts.

There are two versions in the same file:
* imperative
* functional

With a working functional implementation, the idea was to use [Rayon](https://github.com/rayon-rs/rayon) for the parallel version later on, but it didn't work as expected. I had to implement things/traits that I don't understand yet.

At the beginning it took **8 minutes** to find the correct password. Simple code reorganization and obvious optimizations took the time down to **4 minutes**. 

## Parallel with Mutex
In this version the _BaseCounter_ structure uses an internal vector to hold state, to remember where we have been and where we are going to. It just counts.

The _BaseCounter_ is shared between threads using a [Mutex](https://doc.rust-lang.org/book/ch16-03-shared-state.html) smart pointer.

The code finds the correct password after about **3 minutes and 40 seconds**.

## Parallel with RWLock
In this version the _BaseCounter_ structure is the same as in the previous examples.

A Mutex blocks the access to the _BaseCounter_ structure for writting and **reading**. [Read-Write-Lock](https://riptutorial.com/rust/example/24527/read-write-locks) are known to be faster in some scenarios, because it does not block reading operations.

Intuition tells us that this version should be faster than the previous version, but I the code I wrote **does not work** at the moment. While thinking about the problem I found that maybe changing how _BaseCounter_ works could speed up the search.

## Parallel with Arc. BaseCounter is modified
In previous versions, the _BaseCounter_ was **mutable**; the Iterator trait changed the inner state of a vector and returned its value.

In this version, the  _BaseCounter_ is **immutable**; the Iterator trait returns **another** instance of _BaseCounter_ containing the next combination to be tried.

Since there is nothing to mutate, there is no need for neither Mutex nor RWLock.
Simply using an [Atomic Reference Count](https://doc.rust-lang.org/rust-by-example/std/arc.html) (Arc) works perfectly fine.

This version finds the password in about **40 seconds** :-)

# Final thoughts
Since I just started working with Rust concurrency, I am pretty sure that the time result can be improved. If you know how... ¡Let me know! :-)
As one can see in the code, I want to expand the alphabet to make the search process more realistic.


# Postscriptum

A couple of months after working with this code I got enough enlightenment to make the [Rayon](https://github.com/rayon-rs/rayon) variant work.
So I included it in the repository with the other experiments.

